package com.toohotohoot.service;


import com.toohotohoot.repo.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private EmailRepository emailRepository;

    public boolean isDuplicate(String email) {
        return emailRepository.isDuplicate(email);
    }


}
