package com.toohotohoot.annotation;

import com.toohotohoot.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class DuplicateEmailValidator implements ConstraintValidator<NotDuplicateEmail, String> {

    @Autowired
    private EmailService emailService;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !emailService.isDuplicate(value);
    }

}
