package com.toohotohoot.controller;



import com.toohotohoot.model.CandidateRegistrationForm;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping("/")
@RestController
public class WebContrroller {


    @GetMapping("/register")
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView("web/register");
        CandidateRegistrationForm form = new CandidateRegistrationForm();
        modelAndView.addObject("candidateRegistrationForm", form);
        return modelAndView;
    }

    @PostMapping("/register_candidate")
    public ModelAndView registerCandidate(
            @ModelAttribute("candidateRegistrationForm") @Validated CandidateRegistrationForm form,
                                          BindingResult validationResult) {
        ModelAndView modelAndView = new ModelAndView();

        System.out.println("form = " + form);

        if (validationResult.hasErrors()) {
            System.out.println("form is invalid");
            System.out.println("error count = " + validationResult.getErrorCount());
            System.out.println("errors = " + validationResult.getAllErrors());
            modelAndView.setViewName("web/register");
        }else {
            System.out.println("form is valid");
                modelAndView.setViewName("web/register-success");
        }

        return modelAndView;
    }

}
