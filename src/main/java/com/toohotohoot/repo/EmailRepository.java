package com.toohotohoot.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class EmailRepository {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public boolean isDuplicate(String email) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("email", email);
        int count = jdbcTemplate.queryForObject("select count(id) cnt from user " +
                        "where email = :email ",
                params, Integer.class);
        return count > 0;
    }

}
