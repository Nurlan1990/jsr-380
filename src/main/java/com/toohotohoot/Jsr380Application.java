package com.toohotohoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jsr380Application {

	public static void main(String[] args) {
		SpringApplication.run(Jsr380Application.class, args);
	}

}
