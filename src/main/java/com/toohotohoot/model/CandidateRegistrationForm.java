package com.toohotohoot.model;

import com.toohotohoot.annotation.EqualFields;
import com.toohotohoot.annotation.NotDuplicateEmail;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualFields(field1 = "password", field2 = "passwordConfirmation", message = "Sifreler ferqlidir")
public class CandidateRegistrationForm {

    @NotBlank
    @Size(min = 2, max = 50, message = "Ad min 2, max 50 simvol ola biler")
    private String firstName;

    @NotBlank
    @Size(min = 2, max = 50, message = "Ad min 2, max 50 simvol ola biler")
    private String lastName;

    @NotBlank(message = "Email min 6, max 200 simvol ola biler")
    @Email(message = "Email dogru deyil")
    @NotDuplicateEmail()
    private String email;

    @NotNull
    @Size(min = 8, max = 20, message = "Passsword min 8, max 20 simvol ola biler")
    private String password;

    @NotNull
    @Size(min = 8, max = 20)
    private String passwordConfirmation;

    @NotNull
    private String accept;


    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    @Override
    public String toString() {
        return "CandidateRegistrationForm{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                ", accept='" + accept + '\'' +
                '}';
    }
}
