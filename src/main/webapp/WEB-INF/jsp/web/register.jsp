<%--
  Created by IntelliJ IDEA.
  User: student
  Date: 18.10.19
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from www.webstrot.com/html/jobpro/job_light/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Sep 2019 12:39:38 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <link rel="stylesheet" type="text/css" href="css/style_II.css" />
    <link rel="stylesheet" type="text/css" href="css/responsive2.css" />
    <!-- favicon links -->
    <link rel="shortcut icon" type="image/png" href="images/header/favicon.ico" />

    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>

<div class="register_section">
    <!-- register_form_wrapper -->
    <div class="register_tab_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div role="tabpanel">

                        <!-- Nav tabs -->
                        <ul id="tabOne" class="nav register-tabs">
                            <li class="active"><a href="#contentOne-1" data-toggle="tab">personal account <br> <span>i am looking for a job</span></a>
                            </li>
                            <li><a href="#contentOne-2" data-toggle="tab">company account <br> <span>We are hiring Employees</span></a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active register_left_form" id="contentOne-1">
                                <form:form modelAttribute="candidateRegistrationForm" action="register_candidate" method="post">
                                    <div class="jp_regiter_top_heading">
                                        <p>Fields with * are mandatory </p>
                                       <form:errors path="*" cssClass="error"/>
                                    </div>
                                    <div class="row">

                                        <!--Form Group-->

                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <span id="first_name_error" class="error"></span>
                                            <form:input path="firstName" id="first_name" placeholder="First Name*"/>
                                            <form:errors path="firstName" cssClass="error"/>
                                        </div>

                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <span id="last_name_error" class="error"></span>
                                            <form:input path="lastName" id="last_name" placeholder="Last Name*"/>
                                            <form:errors path="lastName" cssClass="error"/>
                                        </div>

                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <span id="email_error" class="error"></span>
                                            <form:input path="email" id="email" placeholder="Email*"/>
                                            <form:errors path="email" cssClass="error"/>
                                        </div>

                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <span id="password_error" class="error"></span>
                                            <form:password path="password" id="password" placeholder=" password*"/>
                                            <form:errors path="password" cssClass="error"/>
                                        </div>
                                        <!--Form Group-->
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <span id="password_confirm_error" class="error"></span>
                                            <form:password path="passwordConfirmation" id="password_confirm" placeholder="re-enter password*"/>
                                            <form:errors path="passwordConfirmation" cssClass="error"/>
                                        </div>

                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="check-box text-center">
                                                <span id="accept_terms_error" class="error"></span>
                                                <form:checkbox path="accept" id="accept_terms" value="0"/>
                                                <label for="accept_terms">I agreed to the <a href="#" class="check_box_anchr">Terms and Conditions</a> governing the use of jobportal</label>
                                                <form:errors path="accept" cssClass="error"/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="login_btn_wrapper register_btn_wrapper login_wrapper ">
                                            <%--                                    <a href="#" class="btn btn-primary login_btn"> register </a>--%>
                                        <input type="submit" class="btn btn-primary login_btn" value="Register">
                                    </div>
                                    <div class="login_message">
                                        <p>Already a member? <a href="#"> Login Here </a> </p>
                                    </div>
                                </form:form>
                            </div>


                            <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that you logout to prevent any un-authorized access to your account</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jp register wrapper end -->

<!-- jp Newsletter Wrapper Start -->
<!-- jp footer Wrapper End -->
<!--main js file start-->
<script src="js/jquery_min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.menu-aim.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/custom_II.js"></script>

<!--main js file end-->
</body>

</html>